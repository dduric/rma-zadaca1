package faks.labosi.zadacalv1;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imgJuninhoMini = (ImageView) findViewById(R.id.imgJuninhoMini);

        imgJuninhoMini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, getString(R.string.JuninhoClubs), Toast.LENGTH_SHORT).show();
            }
        });

        ImageView imgScholesMini = (ImageView) findViewById(R.id.imgScholesMini);

        imgScholesMini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, getString(R.string.ScholesClubs), Toast.LENGTH_SHORT).show();
            }
        });

        ImageView imgRonaldinhoMini = (ImageView) findViewById(R.id.imgRonaldinhoMini);

        imgRonaldinhoMini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, getString(R.string.RonaldinhoClubs), Toast.LENGTH_SHORT).show();
            }
        });
    }

}


